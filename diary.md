# Sensitive absorption measurements in the nearinfrared region using off-axis integrated-cavityoutput spectroscopy

[Baer D S, Paul J B, Gupta M, et al. Sensitive absorption measurements in the near-infrared region using off-axis integrated-cavity-output spectroscopy[J]. Applied Physics B: Lasers and Optics, 2002, 75(2): 261-265.](/papers/s00340-002-0971-z.pdf)

## Theory
The Transmitted laser intensity($I$) though a empty cavity may be expressed by:
$$
I=\frac{I_LC_PT}{2(1-R)}(1-e^{-t/\tau})
$$
where $I_L$ is the incident laser intensity, $C_P$ is a cavity coupling parameter, $R$ and $T$ are the mirror intensity reflection and transmission coefficients, $\tau$ is the characteristic cavity (ringdown) decay time,and $c$ is the speed of light. $C_P$ depends on geometrical factors and the mode quality of the light source and has a value between 0 and 1.
In an empty cavity, the effective path length is:
$$
L_{eff}=\frac{L}{1-R}
$$
With an absorbing gas between the mirrors,  $R$ is replaced by $R'$, given by:
$$
R'=Re^{-\alpha(\nu)}
$$ 
where $\alpha(\nu)$ represents the optical depth (at frequency $\nu$) of the gas over the cavity length.
Comparing with the Beer-Lambert absorption formula for a single pass though the cavity, $I/I_0=e^{-\alpha(\nu)}$, reveals that $I/I_0=R'/R$. Thus in the steady-state, the change $\Delta I=I_L-I$ due to the presence of an absorbing species may be expressed as:
$$
\frac{\Delta I}{I_0}=\frac{GA}{1+GA}
$$
where $A$ is the single-pass absorption, $A=1-e^{-\alpha(\nu)}$ and  $G=R/(1-R)$.

##Experiments
![System Setup](/image/2017111601.jpg)
The characteristic cavity frequency $f_{cavity}=\frac{1}{2\pi\nu}$. The laser wavelength tuning rate was set at a value much less than the characteristic cavity frequency. The detector was a InGaAs photodiode.
![Ringdown measurement](/image/2017111602.jpg)
Cavity ringdown measurements were recorded to determine the effective mirror loss $(1-R)$. The result data indicates that the cavity decay may be described accurately by a single-exponential time constant.The finite frequency response of 1MHz electrical bandwidth of laser current source induced the slight non-exponiental behavior within the first $5-10\mu s$ after the start of the decay.

##Measurements
| Absorbor | LOD       |
|----------|-----------|
| $CO$     | $36ppbV$  |
| $CH_4$   | $1ppbV$   |
| $NH_3$   | $2ppbV$   |
| $C_2H_2$ | $0.3ppbV$ |
The noise equivalent absorption senstivity at a given wavelength was determined from the minimum detectable absorption, effective optical path length in the cell,and detection bandwidth.The detection bandwidth corresponded to the cavity bandwidth $f_{cavity}$ divided by the number of sweeps averaged.The minimum detectable absorption was limited by residual optical interference effects in the cell.

# Ultrasensitive absorption spectroscopy with a high-finesse optical cavity and off-axis alignment

[Paul J B, Lapson L, Anderson J G. Ultrasensitive absorption spectroscopy with a high-finesse optical cavity and off-axis alignment[J]. Applied optics, 2001, 40(27): 4904-4910.](/papers/ao-40-27-4904.pdf)
